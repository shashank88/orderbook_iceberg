# Generates random fuzzed input to validate the program doesn't crash or generate garbage output.
import random

TEST_CASES = 1000

with open("test_data/input_fuzzed.txt", "w") as fp:
    for idx in range(TEST_CASES):
        side = random.choice(["S", "B"])
        order_id = idx
        price = random.randint(1, 9999)
        qty = random.randint(1000, 30000)
        peak = random.choice([None, qty // 2])
        if peak:
            fp.write(",".join(map(str, [side, order_id, price, qty, peak])))
        else:
            fp.write(",".join(map(str, [side, order_id, price, qty])))
        fp.write("\n")
