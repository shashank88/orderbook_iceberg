**To run**

python3 main.py < INPUT_FILE  

python version must be 3.6 or more.

**Possible improvements:**

* Use enum for side. 
* Validations in each part.
* Application server hosting the orderbook.
* f strings throughout instead of format.
* Backwards compatibility with py2.
* Unit tests for validating functions. Currently using test files in data/*
* Inherit from Order -> Limit Order or Iceberg or Market order.

**Data structure and design decisions**

The aim was to make it simple and easier to read so a simple data structure was used here.
I have used maps with lists of orders (in order of arrival) as values and the price as key.
This allows me to simply sort it and iterate through it in any order and 
easily index by price.


**Iceberg calculations**

Deduct executed qty from remaining to get new remaining for both cases:
* Active: Just take the minimum of remaining or peak qty
* Passive: either use the available visible quantity if executed quantity is less
than that OR deduct blocks of peak quantity from invisible and deduct the executed 
qty from that.