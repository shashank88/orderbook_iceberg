from collections import defaultdict
from datetime import datetime
from itertools import zip_longest
from sys import stdin
from typing import Optional


class Order:
    def __init__(self, side: str, order_id: int, price: int, quantity: int, peak: Optional[int] = None):
        self.side = side
        self.order_id = order_id
        self.price = price
        self.total_quantity = quantity
        self.remaining_quantity = quantity
        self.timestamp = datetime.now()

        self.peak = peak
        self.visible_quantity = quantity if not peak else peak

    def __str__(self):
        return (
            f"side:{self.side} "
            f"id:{self.order_id} "
            f"price:{self.price} "
            f"visible_quantity:{self.visible_quantity} "
            f"peak:{self.peak}"
        )

    def execute_qty(self, executed_qty: int, active: bool):
        # Remaining quantity calculations same for limit or iceberg order.
        self.remaining_quantity = self.remaining_quantity - executed_qty
        if self.peak:  # Iceberg Order.
            self.calc_visible_qty_iceberg(executed_qty, active)
        else:  # Limit Order.
            self.visible_quantity = self.remaining_quantity

    def calc_visible_qty_iceberg(self, executed_qty: int, active: bool):
        """
        Calculate the new visible volume.
        Two cases based on the spec section 4.2:
        a) Active iceberg order: Use up as much qty as possible and expose peak
        b) Passive iceberg order:-
        Either the order uses less than visible.
        OR
        It uses visible then releases more 'peak' quantity chunks so it can cover the executed volume.
        :param executed_qty:
        :param active:
        :return:
        """
        if active:  # New Iceberg order, aggressively consume executed quantity and make peak visible.
            self.visible_quantity = min(self.peak, self.remaining_quantity)
            return
        # Passive Iceberg order, consume in one execution.
        if self.visible_quantity > executed_qty:
            self.visible_quantity = self.visible_quantity - executed_qty
        else:
            extra_qty_required = executed_qty - self.visible_quantity
            # Release peak qty to cover the extra executed volume that cannot be covered by visible volume.
            deduct_from_invisible = ((extra_qty_required // self.peak) + 1) * self.peak
            self.visible_quantity = self.visible_quantity + deduct_from_invisible - executed_qty


class Execution:
    def __init__(self, buy_ord_id: int, sell_ord_id: int, price: int, qty: int):
        self.buy_ord_id = buy_ord_id
        self.sell_ord_id = sell_ord_id
        self.price = price
        self.qty = qty

    def __str__(self):
        return f"{self.buy_ord_id},{self.sell_ord_id},{self.price},{self.qty}"


class OrderBook:
    def __init__(self):
        self.buy_map: {int, [Order]} = defaultdict(list)
        self.sell_map: {int, [Order]} = defaultdict(list)

    def __str__(self):
        formatted_book = list()
        formatted_book.append("+{}+".format("-" * 65))
        formatted_book.append("|{:<32}|{:<32}|".format(" BUY", " SELL"))
        formatted_book.append("|{:<10}|{:<13}|{:<7}".format(" Id", " Volume", " Price") * 2 + "|")
        formatted_book.append("+{}+{}+{}".format("-" * 10, "-" * 13, "-" * 7) * 2 + "+")

        for (buy_order, sell_order) in zip_longest(self.unwrap_orders("B"), self.unwrap_orders("S")):
            order_format_str = "|{order_id:>10}|{volume:>13}|{price:>7}"
            order_row = ""
            for order in buy_order, sell_order:
                if order:
                    order_row += order_format_str.format(
                        order_id=order.order_id, volume=order.visible_quantity, price=order.price
                    )
                else:
                    order_row += order_format_str.format(order_id="", volume="", price="")

            formatted_book.append(order_row + "|")

        formatted_book.append("+{}+".format("-" * 65))
        return "\n".join(formatted_book)

    def unwrap_orders(self, side: str) -> [Order]:
        """
        returns a sorted (desc. for buy and asc. for sell) list of all orders.
        :param side:
        :return:
        """
        relevant_items = (
            sorted(self.buy_map.items(), reverse=True) if side == "B" else sorted(self.sell_map.items())
        )
        orders = []
        for _price, orders_at_price in relevant_items:
            orders.extend(orders_at_price)

        return orders

    def can_cross(self, price: int, new_order: Order) -> bool:
        if new_order.side == "S":
            return new_order.price <= price
        return new_order.price >= price

    def cross_order(self, new_order: Order, existing_order: Order) -> Execution:
        """
        Cross two orders which are guaranteed to have a valid cross criteria.
        :param new_order:
        :param existing_order:
        :return:
        """
        total_executed = min(new_order.remaining_quantity, existing_order.remaining_quantity)
        new_order.execute_qty(total_executed, active=True)
        existing_order.execute_qty(total_executed, active=False)

        buy_id, sell_id = (
            (new_order.order_id, existing_order.order_id)
            if new_order.side == "B"
            else (existing_order.order_id, new_order.order_id)
        )
        executed_price = min(existing_order.price, new_order.price)  # Best price execution.
        return Execution(buy_id, sell_id, executed_price, total_executed)

    def cross_orders_at_price(self, new_order: Order, orders: [Order], price: int) -> [Execution]:
        """
        Given a new order and a sorted (desc for buy, asc for sell), cross orders if possible. Also
        cleans the completely crossed dead orders.
        :param new_order:
        :param orders: all orders at price
        :param price:
        :return: List of executions
        """
        if not self.can_cross(price, new_order):
            return []

        executions: [Execution] = []
        for order in orders:
            executions.append(self.cross_order(new_order, order))

        self.clean_dead_orders(orders)
        return executions

    def clean_dead_orders(self, orders: [Order]):
        for order in orders.copy():  # Create a dup to delete from list.
            if order.remaining_quantity == 0:
                orders.pop(0)

    def add_remaining_to_book(self, new_order: Order) -> None:
        if not new_order.remaining_quantity:
            return
        if new_order.side == "S":
            self.sell_map[new_order.price].append(new_order)
        else:
            self.buy_map[new_order.price].append(new_order)

    def add_order(self, new_order: Order) -> [Execution]:
        """
        Add an order to the orderbook
        :param new_order:
        :return: List of executions on adding the order.
        """
        other_side_map = (
            sorted(self.sell_map.items())
            if new_order.side == "B"
            else sorted(self.buy_map.items(), reverse=True)
        )
        executions: [Execution] = []
        for price, orders in other_side_map:
            executions.extend(self.cross_orders_at_price(new_order, orders, price))
            if new_order.remaining_quantity == 0:
                break

        if new_order.remaining_quantity != 0:
            self.add_remaining_to_book(new_order)

        return executions


def parse_raw_order(raw_order: str) -> Order:
    """
    given a valid comma separated string representation of order, deserialize to Order.
    :param raw_order:
    :return: Order
    """
    # Validation can be done here if needed.
    side, order_id, price, quantity, *maybe_peak = raw_order.split(",")
    is_iceberg = len(maybe_peak) > 0
    if is_iceberg:
        return Order(side, int(order_id), int(price), int(quantity), int(maybe_peak[0]))
    else:
        return Order(side, int(order_id), int(price), int(quantity))


def main():
    book = OrderBook()
    for order_raw in stdin:
        order = parse_raw_order(order_raw)
        executions = book.add_order(order)
        [print(execution) for execution in executions]

    print(book)


if __name__ == "__main__":
    main()
